package com.precise.robin.biostat;

import com.google.gson.JsonObject;

/*
* Template object for sending data to the Tizen watch, enforces the cmd parameter
* */

public class CommandPayload {
    private JsonObject jsonObject;

    public CommandPayload(String cmd) {
        jsonObject = new JsonObject();
        jsonObject.addProperty("cmd", cmd);
    }

    // Add additional properties to the command payload
    public void addProperty(String property, String value) {
        jsonObject.addProperty(property, value);
    }

    public void addProperty(String property, int value) {
        jsonObject.addProperty(property, value);
    }

    public void addProperty(String property, boolean value) {
        jsonObject.addProperty(property, value);
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }
}
