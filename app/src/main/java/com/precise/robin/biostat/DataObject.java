package com.precise.robin.biostat;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataObject {
    private List<String> params = new ArrayList<>(Arrays.asList("timestamp", "battery", "cnt", "dataType", "refreshRate", "runNumber", "isCharging"));
    private JsonObject jsonObject;

    // Object that contains the basic fields of a json object that contains data from the watch
    public DataObject(String s) {
        jsonObject = new JsonParser().parse(s).getAsJsonObject();

        for (String param : params) {
            if (!jsonObject.has(param)) {
                Log.e("DataObject", "Missing param: " + param);
                throw new IllegalArgumentException();
            }
        }
    }

    public JsonObject getJson() {
        return jsonObject;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }

    public String toPrettyString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(jsonObject);
    }

    public List<String> getParams() {
        return new ArrayList<>(params);
    }

    public String getTimeStamp() {
        return jsonObject.getAsJsonPrimitive("timestamp").getAsString();
    }

    public double getBatteryLevel() {
        return jsonObject.getAsJsonPrimitive("battery").getAsDouble();
    }

    public int getCount() {
        return jsonObject.getAsJsonPrimitive("cnt").getAsInt();
    }

    public String getDataType() {
        return jsonObject.getAsJsonPrimitive("dataType").getAsString();
    }

    public int getRefreshRate() {
        return jsonObject.getAsJsonPrimitive("refreshRate").getAsInt();
    }

    public int getRunNumber() {
        return jsonObject.getAsJsonPrimitive("runNumber").getAsInt();
    }

    public boolean getBatteryState() {
        return jsonObject.getAsJsonPrimitive("isCharging").getAsBoolean();
    }
}
