package com.precise.robin.biostat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GraphActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static GraphView graphHRM;
    static GraphView graphLinAccX;
    static GraphView graphLinAccY;
    static GraphView graphLinAccZ;
    private static List<GraphView> graphList;
    private static LineGraphSeries<DataPoint> mSeriesHRM;
    private static LineGraphSeries<DataPoint> currentSeriesHRM;
    private static LineGraphSeries<DataPoint> mSeriesLinX;
    private static LineGraphSeries<DataPoint> mSeriesLinY;
    private static LineGraphSeries<DataPoint> mSeriesLinZ;
    private static LineGraphSeries<DataPoint> currentSeriesLinX;
    private static LineGraphSeries<DataPoint> currentSeriesLinY;
    private static LineGraphSeries<DataPoint> currentSeriesLinZ;
    private final static int MAX_POINT_COUNT = 60;
    private static double lastXValueHRM = 0;
    private static double lastXValueLinAccX = 0;
    private static double lastXValueLinAccY = 0;
    private static double lastXValueLinAccZ = 0;


    // Helper function that clears all of the series from the graphs then initialize the graphs
    public static void clearAllSeries() {
        if (graphList != null) {
            for (GraphView gv : graphList) {
                gv.removeAllSeries();
            }

            setUpGraphs();
        }

    }

    public static void changeColor(int newRefreshRate) {
        if (graphList != null) {
            LineGraphSeries<DataPoint> newSeriesHRM = new LineGraphSeries<>();
            newSeriesHRM.setColor(Color.rgb(getRandomColorValue(), getRandomColorValue(), getRandomColorValue()));
            newSeriesHRM.setDrawDataPoints(true);
            newSeriesHRM.setDataPointsRadius(10);
            newSeriesHRM.setThickness(8);
            currentSeriesHRM = newSeriesHRM;
            graphHRM.addSeries(newSeriesHRM);
            newSeriesHRM.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    graphHRM.getViewport().setMinY(dataPoint.getY() - 200000);
                    graphHRM.getViewport().setMaxY(dataPoint.getY() + 200000);
                }
            });

            LineGraphSeries<DataPoint> newSeriesLinAccX = new LineGraphSeries<>();
            newSeriesLinAccX.setColor(Color.rgb(getRandomColorValue(), getRandomColorValue(), getRandomColorValue()));
            newSeriesLinAccX.setDrawDataPoints(true);
            newSeriesLinAccX.setDataPointsRadius(10);
            newSeriesLinAccX.setThickness(8);
            currentSeriesLinX = newSeriesLinAccX;
            graphLinAccX.addSeries(newSeriesLinAccX);

            LineGraphSeries<DataPoint> newSeriesLinAccY = new LineGraphSeries<>();
            newSeriesLinAccY.setColor(Color.rgb(getRandomColorValue(), getRandomColorValue(), getRandomColorValue()));
            newSeriesLinAccY.setDrawDataPoints(true);
            newSeriesLinAccY.setDataPointsRadius(10);
            newSeriesLinAccY.setThickness(8);
            currentSeriesLinY = newSeriesLinAccY;
            graphLinAccY.addSeries(newSeriesLinAccY);

            LineGraphSeries<DataPoint> newSeriesLinAccZ = new LineGraphSeries<>();
            newSeriesLinAccZ.setColor(Color.rgb(getRandomColorValue(), getRandomColorValue(), getRandomColorValue()));
            newSeriesLinAccZ.setDrawDataPoints(true);
            newSeriesLinAccZ.setDataPointsRadius(10);
            newSeriesLinAccZ.setThickness(8);
            currentSeriesLinZ = newSeriesLinAccZ;
            graphLinAccZ.addSeries(newSeriesLinAccZ);
        }


    }

    private static int getRandomColorValue() {
        Random r = new Random();
        return r.nextInt(156) + 100;
    }

    // Helper function that initalize the graphs
    private static void setUpGraphs() {
        // HRM Graph
        mSeriesHRM = new LineGraphSeries<>();
        mSeriesHRM.setColor(Color.rgb(231, 76, 60));
        mSeriesHRM.setDrawDataPoints(true);
        mSeriesHRM.setDataPointsRadius(10);
        mSeriesHRM.setThickness(8);
        currentSeriesHRM = mSeriesHRM;
        graphHRM.addSeries(mSeriesHRM);

        graphHRM.getViewport().setXAxisBoundsManual(true);
        graphHRM.getViewport().setMinX(0);
        graphHRM.getViewport().setMaxX(MAX_POINT_COUNT);
        graphHRM.getViewport().setXAxisBoundsManual(false);

        graphHRM.getViewport().setYAxisBoundsManual(true);
        graphHRM.getViewport().setMinY(100000);
        graphHRM.getViewport().setMaxY(4500000);
        graphHRM.getViewport().setScalableY(true);
        mSeriesHRM.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                graphHRM.getViewport().setMinY(dataPoint.getY() - 200000);
                graphHRM.getViewport().setMaxY(dataPoint.getY() + 200000);
            }
        });

        // Lin Acc Graph
        mSeriesLinX = new LineGraphSeries<>();
        mSeriesLinX.setColor(Color.rgb(165, 105, 189));
        mSeriesLinX.setDrawDataPoints(true);
        mSeriesLinX.setDataPointsRadius(10);
        mSeriesLinX.setThickness(8);
        currentSeriesLinX = mSeriesLinX;
        graphLinAccX.addSeries(mSeriesLinX);
        graphLinAccX.getViewport().setXAxisBoundsManual(true);
        graphLinAccX.getViewport().setMinX(0);
        graphLinAccX.getViewport().setMaxX(MAX_POINT_COUNT);
        graphLinAccX.getViewport().setScalableY(true);

        mSeriesLinY = new LineGraphSeries<>();
        mSeriesLinY.setColor(Color.rgb(93, 173, 226));
        mSeriesLinY.setDrawDataPoints(true);
        mSeriesLinY.setDataPointsRadius(10);
        mSeriesLinY.setThickness(8);
        currentSeriesLinY = mSeriesLinY;
        graphLinAccY.addSeries(mSeriesLinY);
        graphLinAccY.getViewport().setXAxisBoundsManual(true);
        graphLinAccY.getViewport().setMinX(0);
        graphLinAccY.getViewport().setMaxX(MAX_POINT_COUNT);
        graphLinAccY.getViewport().setScalableY(true);

        mSeriesLinZ = new LineGraphSeries<>();
        mSeriesLinZ.setColor(Color.rgb(69, 179, 157));
        mSeriesLinZ.setDrawDataPoints(true);
        mSeriesLinZ.setDataPointsRadius(10);
        mSeriesLinZ.setThickness(8);
        currentSeriesLinZ = mSeriesLinZ;
        graphLinAccZ.addSeries(mSeriesLinZ);
        graphLinAccZ.getViewport().setXAxisBoundsManual(true);
        graphLinAccZ.getViewport().setMinX(0);
        graphLinAccZ.getViewport().setMaxX(MAX_POINT_COUNT);
        graphLinAccZ.getViewport().setScalableY(true);


        graphList = new LinkedList<>();
        graphList.add(graphHRM);
        graphList.add(graphLinAccX);
        graphList.add(graphLinAccY);
        graphList.add(graphLinAccZ);

        for (GraphView gv : graphList) {
            GridLabelRenderer gridLabel = gv.getGridLabelRenderer();
            gridLabel.setHorizontalAxisTitle("seconds (s)");
        }
    }

    // Check if the dataObject's type and process them to be appended to the graph
    public static void processData(List<DataObject> dataObjectList) {

        for (DataObject dataObject : dataObjectList) {
            if (dataObject instanceof HrmDataObject) {
                appendHRMData(dataObject);
            } else if (dataObject instanceof LinAccDataObject) {
                appendLinAccData(dataObject);
            }
        }
    }

    // Plot the intensity of the HRM data against the count
    public static void appendHRMData(DataObject dataObject) {
        HrmDataObject hrmDataObject = (HrmDataObject) dataObject;
        if (currentSeriesHRM != null) {

            currentSeriesHRM.appendData(new DataPoint(lastXValueHRM, hrmDataObject.getIntensity()), true, MAX_POINT_COUNT * hrmDataObject.getRefreshRate());
            lastXValueHRM += 1.0 / hrmDataObject.getRefreshRate();
        }
    }

    // Plot the acceleration of the lin acc sensors to their respective graph
    public static void appendLinAccData(DataObject dataObject) {
        LinAccDataObject linAccDataObject = (LinAccDataObject) dataObject;
        if (currentSeriesLinX != null) {

            currentSeriesLinX.appendData(new DataPoint(lastXValueLinAccX, linAccDataObject.getXAcc()), true, MAX_POINT_COUNT * linAccDataObject.getRefreshRate());
            lastXValueLinAccX += 1.0 / linAccDataObject.getRefreshRate();
        }
        if (currentSeriesLinY != null) {
            currentSeriesLinY.appendData(new DataPoint(lastXValueLinAccY, linAccDataObject.getYAcc()), true, MAX_POINT_COUNT * linAccDataObject.getRefreshRate());
            lastXValueLinAccY += 1.0 / linAccDataObject.getRefreshRate();
        }
        if (currentSeriesLinZ != null) {
            currentSeriesLinZ.appendData(new DataPoint(lastXValueLinAccZ, linAccDataObject.getZAcc()), true, MAX_POINT_COUNT * linAccDataObject.getRefreshRate());
            lastXValueLinAccZ += 1.0 / linAccDataObject.getRefreshRate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_graph);

        // Action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        graphHRM = findViewById(R.id.graphHRM);
        graphLinAccX = findViewById(R.id.graphLinAccX);
        graphLinAccY = findViewById(R.id.graphLinAccY);
        graphLinAccZ = findViewById(R.id.graphLinAccZ);
        setUpGraphs();

    }

    // Cycles the activities
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_console) {

            Intent intent = new Intent(this, ConsoleActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_setting) {

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
