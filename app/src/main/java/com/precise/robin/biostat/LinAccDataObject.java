package com.precise.robin.biostat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Object that contains the parameteres for the linear acceleration data
public class LinAccDataObject extends DataObject {
    private List<String> params = new ArrayList<>(Arrays.asList("x", "y", "z"));

    public LinAccDataObject(String s) throws IllegalArgumentException {
        super(s);

        for (String param : params) {
            if (!super.getJson().has(param)) {
                throw new IllegalArgumentException();
            }
        }
        params.addAll(super.getParams());
    }

    @Override
    public List<String> getParams() {
        return new ArrayList<>(params);
    }

    public double getXAcc() {
        return super.getJson().getAsJsonPrimitive("x").getAsDouble();
    }

    public double getYAcc() {
        return super.getJson().getAsJsonPrimitive("y").getAsDouble();
    }

    public double getZAcc() {
        return super.getJson().getAsJsonPrimitive("z").getAsDouble();
    }
}
