package com.precise.robin.biostat;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

// Parser that parses individual lines, check if a HRM or lin acc dataObject
public class JSonParser {
    private List<DataObject> jsonList;
    private List<CommandObject> commandList;
    private boolean isData = false;
    private boolean isCommand = false;

    public JSonParser(String s) {
        String[] unprocessedStrings = s.split("\\r?\\n");

        jsonList = new ArrayList<>();
        commandList = new ArrayList<>();
        for (String jsonString : unprocessedStrings) {
            DataObject newDataObj = null;
            CommandObject newCommandObj = null;
            try {
                newDataObj = new HrmDataObject(jsonString);
            } catch (IllegalArgumentException e) {
                try {
                    newDataObj = new LinAccDataObject(jsonString);
                } catch (IllegalArgumentException j) {
                    try {
                        newCommandObj = new CommandObject(jsonString);
                    } catch (IllegalArgumentException g) {
                        Log.i("JSONParser", "Received unknown string, unable to parse: " + s);

                    }


                }
            }
            if (newDataObj != null) {
                jsonList.add(newDataObj);
                isData = true;
            } else if (newCommandObj != null) {
                commandList.add(newCommandObj);
                isCommand = true;
            }
        }

    }

    // Returns the list of DataObject that are parsed
    public List<DataObject> getJsonList() {
        if (isData) {
            return jsonList;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean isData() {
        return isData;
    }

    public List<CommandObject> getCommandList() {
        if (isCommand) {
            return commandList;
        } else {
            throw new IllegalStateException();
        }
    }

    public boolean isCommand() {
        return isCommand;
    }
}
