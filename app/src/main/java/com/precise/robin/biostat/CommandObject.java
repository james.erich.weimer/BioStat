package com.precise.robin.biostat;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.precise.robin.biostat.CommandParser.acceptedFileFormat;

/*
 *  Object used to wrap receiving commands from Tizen watch, with only requirement of a cmd field
 * */
public class CommandObject {
    private List<String> params;
    private JsonObject jsonObject;

    public CommandObject(String s) {
        // Parse string and set the requirement of the cmd field
        jsonObject = new JsonParser().parse(s).getAsJsonObject();
        params = new ArrayList<>(Arrays.asList("cmd"));


        // Check if all params are fulfilled
        for (String param : params) {
            if (!jsonObject.has(param)) {
                Log.e("CommandObject", "Missing param: " + param);
                throw new IllegalArgumentException();
            }
        }

        // Reset the params to take care of the additional params other than cmd
        params = new ArrayList<>(jsonObject.keySet());
    }

    public JsonObject getJson() {
        return jsonObject;
    }

    @Override
    public String toString() {
        return jsonObject.toString();
    }

    public String toPrettyString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(jsonObject);
    }

    public List<String> getParams() {
        return new ArrayList<>(params);
    }

    // Return the type of command
    public String getCommand() {
        return jsonObject.getAsJsonPrimitive("cmd").getAsString();
    }

    // Return the amount of characters in the file for a particular file preamble command
    //TODO: Change against transfer protocol parameter is present as int before returning
    public int getCount(String memberName) {
        return jsonObject.getAsJsonPrimitive(memberName).getAsInt();
    }

    // Return the file type if present
    //TODO: Change against transfer protocol if file parameter is present before returning
    public String getFile() {
        return jsonObject.getAsJsonPrimitive("file").getAsString();
    }

    // Return the data matching the file type parameter
    public String getData(String fileType) {
        if (acceptedFileFormat.contains(fileType)) {
            return jsonObject.getAsJsonPrimitive(fileType).getAsString();
        } else {
            throw new IllegalStateException("Not an accepted file format");
        }
    }
}
