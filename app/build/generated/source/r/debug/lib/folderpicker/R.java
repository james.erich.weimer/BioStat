/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package lib.folderpicker;

public final class R {
    public static final class color {
        public static final int colorAccent = 0x7f050026;
        public static final int colorPrimary = 0x7f050027;
        public static final int colorPrimaryDark = 0x7f050028;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin = 0x7f06004b;
        public static final int activity_vertical_margin = 0x7f06004c;
    }
    public static final class drawable {
        public static final int fp_file = 0x7f070074;
        public static final int fp_folder = 0x7f070075;
        public static final int fp_ic_action_add = 0x7f070076;
        public static final int fp_ic_action_back = 0x7f070077;
        public static final int fp_ic_action_cancel = 0x7f070078;
        public static final int fp_ic_action_up = 0x7f070079;
        public static final int fp_outline_black_1dp = 0x7f07007a;
    }
    public static final class id {
        public static final int fp_btn_new = 0x7f08005f;
        public static final int fp_btn_select = 0x7f080060;
        public static final int fp_buttonsLayout = 0x7f080061;
        public static final int fp_iv_icon = 0x7f080062;
        public static final int fp_listView = 0x7f080063;
        public static final int fp_tv_location = 0x7f080064;
        public static final int fp_tv_name = 0x7f080065;
        public static final int fp_tv_title = 0x7f080066;
    }
    public static final class layout {
        public static final int fp_filerow = 0x7f0a0032;
        public static final int fp_main_layout = 0x7f0a0033;
    }
    public static final class string {
        public static final int allow_storage_permission = 0x7f0d0025;
        public static final int app_name = 0x7f0d0026;
        public static final int cancel = 0x7f0d002f;
        public static final int location = 0x7f0d0068;
        public static final int neww = 0x7f0d006f;
        public static final int select = 0x7f0d008b;
        public static final int title = 0x7f0d0092;
        public static final int up = 0x7f0d0097;
    }
}
